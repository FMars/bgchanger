(function($) {

    var methods = {
        init: function(options) {

            var settings = $.extend({
                duration:   5000,
            }, options);

            var imgs = $(this).children('img');

            // set default css
            $(this).children().hide()

            // show first slide
            $(this).css('background-image', 'url(' + $(imgs[0]).attr('src') + ')');

            // main loop
            var i  = 1;
            var th = this;
            (function loop(){
                setTimeout(function(){
                    $(th).css('background-image', 'url(' + $(imgs[i % imgs.length]).attr('src') + ')');
                    loop();
                    i++;
                }, settings.duration);
            })();

            return this;
        },
    };

    $.fn.bgChanger = function(method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === "object" || !method) {
            return methods.init.apply( this, arguments );
        } else {
            $.error("Method '" +  method + "' is not exists.");
        } 
    };

})(jQuery);