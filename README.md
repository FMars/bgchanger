# BGChanger
**BGChanger** - is simple jquery plugin for changing ``background-image`` css property of any html element.

# Installation
Include plugin after jquery library
```html
<script src="bgchanger.jquery.js"></script>
```

# Usage
Create any html element and add inside images:

```html
<div id="bgChanger">
    <img src="image_1.jpg" />
    <img src="image_2.jpg" />
    <img src="image_3.jpg" />
</div>
```
And then:
```js
$('#bgChanger').bgChanger();
```
It's all what you need.

# Properties

### duration :integer | 5000 - default
Duration in milliseconds. Needs for set duration of changing slides.